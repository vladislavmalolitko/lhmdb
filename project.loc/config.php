<?php

define ("ROOT_PATH", dirname(__FILE__,1));
const CLASSES_PATH = ROOT_PATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR;
const FUNCTIONS_PATH = ROOT_PATH . DIRECTORY_SEPARATOR . "functions_sql" . DIRECTORY_SEPARATOR;
const TEMPLATES_PATH = ROOT_PATH . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR;
const DB_NAME = "football_news";
const DB_USER = 'user';
const DB_PASSWORD = '1111';

session_start();

try{
    $dsn = 'mysql:host=localhost;port=3306;dbname='.DB_NAME.';charset=utf8';
    $pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
    //echo "Successful connection";
}catch (PDOException $exception){
    echo "DB Connection Failed: ".$exception->getMessage();
}

error_reporting(E_ALL);
ini_set('display_errors', 1);

//$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_COLUMN);
//$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
//$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);